


<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function (){
    return view('welcome');
});


Route::get('/abc', function (){
    return view('grade/create');
});


Route::get('/test',function()
{

    return view ('test');
});




Route::post('/process',function(Request $request)
{
 $name=$_REQUEST['name'];
    $mark=$_REQUEST['mark'];

    if(strlen($request->name)>0 || strlen($request->mark)>0) {
        if ($mark >= 80 && $mark <= 100) {
            echo " Name is $name  Grade is A+";
        }

        if ($mark >= 60 && $mark < 70) {
            echo " Name is $name  Grade is A";
        }
        if ($mark >= 50 && $mark < 60) {
            echo " Name is $name  Grade is B";
        }
        if ($mark >= 40 && $mark < 50) {
            echo " Name is $name  Grade is C";
        }
        if ($mark >= 33 && $mark < 40) {
            echo " Name is $name  Grade is D";
        }
        if ($mark >= 0 && $mark < 33) {
            echo " Name is $name  Grade is F";
        }
        if ($mark < 0 || $mark > 100) {
            echo "Please Input Valid Number";
        }
    }
    else
    {

        echo "Some Input is required";
    }

    return view(['name'=> $name, 'mark' => $mark]);
});
